var users, access;

users  = {
  mom: "blah",
  dad: "blah",
  kiki: "yay",
  guest: "noaccess"
};
access = {
  GET: {mom: true, dad: true, kiki: true, guest: false},
  PUT: {mom: true, dad: true, kiki: false, guest: false},
  DELETE: {mom: true, dad: true, kiki: false, guest: false}
};

module.exports = {
  checker: function(req, username, password) {
    var allowed = access[req.method];
    if (allowed[username] && users[username] === password) {
      return username;
    }
  }
};