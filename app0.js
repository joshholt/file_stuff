/*globals process*/
var  HTTP  			= require('http')
		,Stack 			= require('stack')
		,Creationix = require('creationix')
		,root				= process.cwd()
		,port				= process.env.PORT || 8080;

HTTP.createServer(Stack(
	Creationix.log(),
	Creationix.indexer("/", root),
	Creationix.static("/", root)
)).listen(port);

console.log("Serving %s at http://localhost:%s/", root, port);