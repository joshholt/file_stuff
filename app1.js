/*globals process __dirname*/
var HTTPS = require('https'),
    Stack = require('stack'),
    Creationix = require('creationix'),
    FS = require('fs'),
    root = process.cwd(),
    keys = __dirname + "/keys/",
    opts = {
      key: FS.readFileSync(keys +  "my-key.pem"),
      cert: FS.readFileSync(keys + "my-cert.pem")
    },
    auth = require("./lib/auth"),
    port = process.env.PORT || 8433;

HTTPS.createServer(opts, 
  Stack(
    Creationix.log(),
    Creationix.auth(auth.checker, "Home Files"),
    Creationix.uploader("/", root),
    Creationix.deleter("/", root),
    Creationix.indexer("/", root), 
    Creationix.static("/", root)
  )
).listen(port);

console.log("Serving %s at https://localhost:%s/", root, port);
